//import './graphql.css';
import React from 'react';
import { render } from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import App from './graphql';

/*
import Analytics from 'react-router-ga';
import { BrowserRouter } from 'react-router-dom';

ReactDOM.render(<BrowserRouter>
    <Analytics id="UA-111111111-1">
    <App />
    </Analytics>
    </BrowserRouter>, document.getElementById('root')); 
*/

//import BasicExample from './routing';
//render(<BasicExample/>, document.getElementById('root'));

render(<App/>, document.getElementById('root'));
registerServiceWorker();